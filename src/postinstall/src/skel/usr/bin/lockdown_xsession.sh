#!/bin/bash

# Switch off sounds
amixer -c 1 -- sset Master playback off

# Before installing
if test "$USER" = "student" || test "$USER" = "online"
then

	# Do not do this... crashes gnome's panel.
	# We already remove entire directories anyways in /usr/bin/lockdown_pre_xsession
	# gsettings list-schemas | xargs -n 1 gsettings reset-recursively

	# List with:  gsettings list-recursively | grep screensaver
	gsettings set org.gnome.desktop.screensaver lock-enabled false
	gsettings set org.gnome.desktop.lockdown disable-lock-screen true
	gsettings set org.gnome.desktop.lockdown disable-print-setup true
	gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend false
	gsettings set org.gnome.gnome-panel.lockdown locked-down true

	# Do not automatically maximize windows
	# This is confusing to users that don't know Window Management
	# https://askubuntu.com/questions/1108528/firefox-window-always-starting-maximized
	gsettings set org.gnome.mutter auto-maximize false

fi

xdg-mime default vlc.desktop audio/x-scpls

# This does not work because stupid XDG will complain about
# unknown DM
# xdg-settings set default-web-browser firefox.desktop

