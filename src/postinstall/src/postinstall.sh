#!/bin/bash

if [ "$1" == '--no-reboot' ]
then
	NO_REBOOT="yes"
else
	NO_REBOOT="no"
fi

set -e

# Create a utility script that will re-download the postinstall
echo "#!/bin/sh" > "/root/rerun-postinstall.sh"
echo "rm -Rf /root/postinstall" >> "/root/rerun-postinstall.sh"
echo "mkdir -p /root/postinstall" >> "/root/rerun-postinstall.sh"
echo "cd /root/postinstall" >> "/root/rerun-postinstall.sh"
echo "wget -O postinstall.tar.gz http://192.168.10.1/postinstall.tar.gz" >> "/root/rerun-postinstall.sh"
echo "tar xfz postinstall.tar.gz" >> "/root/rerun-postinstall.sh"
echo "./postinstall.sh --no-reboot" >> "/root/rerun-postinstall.sh"

chmod +x /root/rerun-postinstall.sh

export DEBIAN_FRONTEND=noninteractive

# This sets $SCRIPT_ROOT to a default if unset
SCRIPT="`readlink -e $0`"
: ${SCRIPT_ROOT:="`dirname $SCRIPT`"}

. "${SCRIPT_ROOT}/utils.sh"

# Stop lightdm
/etc/init.d/lightdm stop

# Go to the root
cd /root/postinstall

. utils.sh

copy_skel etc/

# If we are running on a previous installation, the computer has an ID
if [ -f /etc/computer_label_id ]
then
	label=`cat /etc/computer_label_id`
	echo "$label-fair-client" > /etc/hostname
else
	echo "fair-client" > /etc/hostname
fi

# Software installation and removal
# Needs to happen before user configuration because some packages
# have special user groups that they add.

# Make sure previous abn. terminated instances of DPKG are fixed
echo "Ensuring consistency of package installation"
dpkg --configure -a
apt-get -y -f install

echo "Update repositories from server"
apt-get update

# THIS IS NOW SKIPPED AND ALL INSTALLATION MOVED TO preseed.cfg
# Install extra packages
# https://superuser.com/questions/164553/automatically-answer-yes-when-using-apt-get-install
#echo "Install extra software"
#apt-get install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" `cat packages.install.lst | egrep "^[^#]+$"`

# Remove keyring stuff for password management
rm -f /etc/xdg/autostart/gnome-keyring-*

# These ones do not support noninteractive mode
echo "Retry some package configurations that do not allow noninteractive mode"
dpkg --configure -a

# Remove packages - do this AFTER installing, because some meta packages
# like edubuntu-desktop will install Thunderbird and the likes...
echo "Remove unwanted packages"
apt-get remove -q -y `cat packages.remove.lst | egrep "^[^#]+$"`

echo "Remove unecessary packages"
apt-get -q -y autoremove


# Remove student from groups
# Do not fail if student is already removed from troups
useradd -m -U -s /bin/bash student || true
usermod student --comment "Student"
deluser student sudo || true
deluser student lpadmin || true
deluser student sambashare || true
adduser student floppy || true
adduser student cdrom || true
adduser student plugdev || true
echo "student:student" | chpasswd || true

# Create a teacher account
useradd -m -U -s /bin/bash teacher || true
usermod teacher --comment "Teacher"
echo "teacher:ilovestudents" | chpasswd || true
adduser teacher sudo
adduser teacher adm
adduser teacher dialout
adduser teacher fax
adduser teacher cdrom
adduser teacher floppy
adduser teacher tape
adduser teacher dip
adduser teacher video
adduser teacher plugdev
adduser teacher lpadmin
adduser teacher sambashare

# MAKE NETWORK CONFIGURATION STATIC
# All of this is unnecessary because it's done perfectly by the debian installer!!
NETWORK_INTERFACE=`ls -1 /sys/class/net | grep ^en | head -n1`

echo ""
echo "Found $NETWORK_INTERFACE"
echo ""

# Copy main network configuration that disables the primary device
# from admin'ed in NetworkManager
copy_skel etc/NetworkManager/NetworkManager.conf $NETWORK_INTERFACE

# Remove system-wide connection Wired connection 1 - because it's just
# a bit confusing, since it's not being used (overwritten by netplan config)
rm -f "/etc/NetworkManager/system-connections/Wired connection 1"

copy_skel etc/netplan/98-fair.yaml $NETWORK_INTERFACE

# Then apply the new settings
netplan --debug apply

# Removing programs from autostart
#rm -f /etc/xdg/autostart/update-notifier.desktop
#rm -f /etc/xdg/autostart/nm-applet.desktop
#rm -f /etc/xdg/autostart/gnome-power-manager.desktop
#rm -f /etc/xdg/autostart/bluetooth-applet.desktop
rm -f /etc/xdg/autostart/evolution-alarm-notify.desktop
rm -f /etc/xdg/autostart/ubuntuone-launch.desktop


# Create home dir backups and reset on reboot
chmod 755 /etc/init.d/create_homes

# These are re-created entirely in case of updates, such that previously
# distributed desktop shortcuts etc. can be deleted and renamed
rm -Rf /root/student
rm -Rf /root/teacher

copy_skel root

systemctl enable create_homes

# Allow the teacher accounts to access root via SSH and use uniform SSH key
# for remote access.
# Recall, these centers are offline, hence this approach for now.
mkdir -p /root/.ssh
cat server_id_rsa.pub > /root/.ssh/authorized_keys
cat /root/teacher/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

chmod -R 700 /root/.ssh

# Disable lightdm sound
if [ -f /usr/share/sounds/ubuntu/stereo/system-ready.ogg ]
then
	mv /usr/share/sounds/ubuntu/stereo/system-ready.ogg /usr/share/sounds/ubuntu/stereo/system-ready.ogg.old
fi

# NFS shares

mkdir -p /var/movies/
mkdir -p /var/materials/

chmod +x /etc/init.d/mount_shares
systemctl enable mount_shares
systemctl start mount_shares

# Make student the default login
#sudo /usr/lib/lightdm/lightdm-set-defaults --autologin student
gpasswd -a student nopasswdlogin

# Do not require any further permissions for mounting and unmounting
# filesystems, affects USB fash
copy_skel usr/share/polkit-1/actions/org.freedesktop.udisks2.policy

# Copy our own binaries and make them executable
copy_skel usr/bin/
chmod +x /usr/bin/lockdown*

# Disable hotspot login windows
copy_skel etc/NetworkManager/conf.d/20-connectivity-ubuntu.conf


# Remove MOTD spam from online sources
rm -f /etc/update-motd.d/10-help-text
rm -f /etc/update-motd.d/50-motd-news
rm -f /etc/update-motd.d/80-esm
rm -f /etc/update-motd.d/80-livepatch


#############################
# local/ folder instructions
#############################

# Remove extra packages for center
if [ -f local/packages.remove.lst ]
then
	apt-get remove -q -y `cat local/packages.remove.lst | egrep "^[^#]+$"`
	apt-get -q -y autoremove
fi
# Install extra packages for center
if [ -f local/packages.install.lst ]
then
	apt-get install -q -y `cat local/packages.install.lst | egrep "^[^#]+$"`
fi

# Copy etc structure over the existing one.
mkdir -p /opt

copy_skel opt/

# Yes yes, these apps should share some modules :)
copy_skel opt/fair-apps/shutdown/settings_customize.py $NETWORK_INTERFACE
copy_skel opt/fair-apps/sharescreen/settings_customize.py $NETWORK_INTERFACE
copy_skel opt/fair-apps/clientmanager/settings_customize.py $NETWORK_INTERFACE

# Make LightDM scripts executalbe
chmod +x /etc/lightdm/*sh

# A utility script that adds a configuration to force X to use VESA
cp force_vesa* /root/
chmod +x /root/force_vesa*.sh

# After copying in the /etc structure, a new keyboard layout may have been set
# and because of a weird bug, we need to re-run this configuration to
# regenerate some init image for kernel and then it will work after reboot
dpkg-reconfigure -phigh keyboard-configuration

#Install Chichewa spellchecking dictionary in LibreOffice
copy_skel usr/share/fair
unopkg add --suppress-license --shared -f /usr/share/fair/libreoffice-chicewa-dictionary.oxt

# Execute final local configuration script
cd local
if [ -d skel ]
then
	cp -Rf skel/* /
fi
bash final.sh
cd ..

# Create home directories for teachers and students
systemctl start mount_shares
systemctl start create_homes

# DONE! --remove from rc.local

sleep 1s

# apt-get remove -y -q plymouth-theme-edubuntu

cp register_computer /root/register_computer

echo ". /root/register_computer" > /etc/rc.local

echo ""
echo "All done without errors :)"
echo ""

if [ ! $NO_REBOOT = "yes" ]
then
	reboot
else
	echo "Skipping reboot."
fi
