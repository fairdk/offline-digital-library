#!/bin/bash

# Here we set up the NFS server, and the directories that it depends on.
# We also create the teacher account here.

echo "---------------------------------------"
echo "Installing NFS server                  "
echo "---------------------------------------"

#echo "Blacklisting IPv6 modules due to a bug"

#sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash ipv6.disable=1"/g' /etc/default/grub

#if [ -f /proc/sys/net/ipv6/conf/all/disable_ipv6 ]
#then
#	echo "Switching off IPv6"
#	echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
#fi

# Ensure there's a teacher account
# This is not used anymore since the teacher has UID 1000 on the client.
#if [ ! -d /home/teacher ]
#then
#	echo "Creating a teacher account and a shared NFS folder"
#	useradd -m -U -s /bin/bash teacher
#	echo "teacher:${TEACHER_PASSWORD}" | chpasswd
#fi

NFS_TEACHER_SHARE=/var/materials

# Ensure it has a materials folder
if [ ! -d $NFS_TEACHER_SHARE ]
then
	mkdir -p $NFS_TEACHER_SHARE
	chown -R 1000.1000 $NFS_TEACHER_SHARE
fi

echo "Put documents here, and students will be able to access them from their Desktop" > $NFS_TEACHER_SHARE/README.txt
chown -R 1000.1000 $NFS_TEACHER_SHARE

# The server should be installed BEFORE modifying /etc/exports, otherwise a prompt appears with no automatic way to circumvent it...
apt-get install -y -q nfs-kernel-server

# Define which folders to share
echo "${FAIR_DRIVE_MOUNTPOINT}/ubuntu/ *(ro,no_subtree_check,no_root_squash)" > /etc/exports
echo "${FAIR_DRIVE_MOUNTPOINT}/data/movies/ *(ro,no_subtree_check,no_root_squash)" >> /etc/exports
echo "$NFS_TEACHER_SHARE *(rw,no_subtree_check,no_root_squash)" >> /etc/exports

# Ensure that the export is created
exportfs -a

systemctl restart nfs-kernel-server
