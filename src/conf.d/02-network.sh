echo "---------------------------------------"
echo "Configuring the network"
echo "---------------------------------------"

NETWORK_INTERFACE=`ls /sys/class/net | head -n1`

echo "Auto-detected network interface to use: $NETWORK_INTERFACE"
echo ""
echo "You can select another interface, type a number from the list:"
echo ""

ls -1 /sys/class/net | nl

CHOICES=`ls -1 /sys/class/net | wc -l`

echo
echo "Auto-selecting $NETWORK_INTERFACE in 10 seconds..."

set +e
read -t 10 CHOICE
exit_status=$?
set -eu

if [ $exit_status -eq 0 ]
then
    if ( ! ( echo "$CHOICE" | grep -q "[[:digit:]]" ) ) || [ "$CHOICE" -gt "$CHOICES" ]
    then
        echo "Invalid choice"
        exit 1
    fi
else
    CHOICE=1
fi

NETWORK_INTERFACE=`ls -1 /sys/class/net | head -n$CHOICE`

echo "Selected $NETWORK_INTERFACE"
echo ""

# Store in case we need it later
mkdir -p /etc/fair
echo -n $NETWORK_INTERFACE > /etc/fair/network_interface


# Copy main network configuration
copy_skel etc/netplan/99-fair.yaml $NETWORK_INTERFACE
copy_skel etc/netplan/01-network-manager-all.yaml

# Disable hotspot login windows
copy_skel etc/NetworkManager/conf.d/20-connectivity-ubuntu.conf

# Then apply the new settings
netplan --debug apply

#############################################################
# HOST NAMES
#############################################################

if ! grep "$FAIR_SERVER_HOSTNAME" /etc/hosts -q
then
	echo "127.0.0.1 $FAIR_SERVER_HOSTNAME" >> /etc/hosts
fi

if ! grep "192.168.10.1" /etc/hosts -q
then
	echo 192.168.10.1 fair-server repository.fair intranet intranet.fair fair repository.fair wikipedia.fair repo dvd.fair khan.fair static.fair khan kiwix.fair kolibri.fair >> /etc/hosts
fi

if ! grep "archive.canonical.com" /etc/hosts -q
then
	echo 192.168.10.1 archive.canonical.com >> /etc/hosts
fi


# Set the hostname permanently
echo "$FAIR_SERVER_HOSTNAME" > /etc/hostname
# Set the hostname right now, so we don't have to wait for a reboot
hostname "$FAIR_SERVER_HOSTNAME"


echo "---------------------------------------"
echo "Installing dnsmasq DHCP & DNS server   "
echo "---------------------------------------"

# Disable systemd-resolved DNS server
copy_skel etc/systemd/resolved.conf
systemctl restart systemd-resolved
rm /etc/resolv.conf
touch /etc/resolv.conf

# DHCP information about DNS servers should be written
# in /run/systemd/resolve/resolv.conf - so can use this
# on for configuring dnsmasq to upstream DNS
# resolv-file=/etc/resolv.personal

# Not needed in 18.04, right?
# remove resolvconf so it doesn't write 127.0.0.1 in /etc/resolv.conf
# apt-get remove -y -q resolvconf

# Note: The command is a test of the local Ubuntu repository; it will fail if the server can't find the installation packages.
apt-get install -y -q dnsmasq

echo "Creating configuration file /etc/dnsmasq.d/fair"
# This file configures the DHCP server, including which interface, which range of IP, and the location of the TFTP server
copy_skel etc/dnsmasq.d/fair $NETWORK_INTERFACE

# Create the TFTP dir now, or else DSNMasq complains (the directory is populated later)
mkdir -p /var/tftp

# Fix dnsmasq starting before Network Manager
# https://bugs.launchpad.net/ubuntu/+source/dnsmasq/+bug/1531184
copy_skel lib/systemd/system/dnsmasq.service
# This one does not work anymore since networkd handles stuff
# sed -i 's/After=network.target/After=NetworkManager-wait-online.service/' /lib/systemd/system/dnsmasq.service

systemctl daemon-reload

/etc/init.d/dnsmasq restart


echo "---------------------------------------"
echo "Installing NTP time server             "
echo "---------------------------------------"
# Deleting old config prevents prompt when installing ntp.
rm -f /etc/ntp.conf

apt-get install -y -q ntp
echo "Copying NTP configuration..."
copy_skel etc/ntp.conf
/etc/init.d/ntp restart


