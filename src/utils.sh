#!/bin/bash

# Easily make a replacement and escape the input and output strings
function sedeasy {
  sed -i "s/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/$(echo $2 | sed -e 's/[\/&]/\\&/g')/g" $3
}

# Delete a line containing $1 in file $2
function sedeasy_delete {
  sed -i "/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/d" $2
}

# Copy $1 to its destination
# Replace all occurrences of {{2}} with argument at 2nd position etc.
# ...meaning {{1}} is replaced by file name.
function copy_skel {
  echo "Copying "${FAIR_INSTALL_SKEL}$1" to "/$1""
  cp -fd "${FAIR_INSTALL_SKEL}$1" "/$1"

  for ((i=1; i<=$#; i++))
  do
    sedeasy "{{$i}}" ${!i} "/$1"
  done

}
