Offline Digital Library
=======================

[![Docs](https://readthedocs.org/projects/offline-digital-library/badge/?version=latest)](http://offline-digital-library.readthedocs.org/)

This project automatically configures an Ubuntu-based server for a Digital Library.
Client computers for students/users are then installed through PXE netboot.
The server contains a number of offline educational resources, together with
the full Ubuntu software repositories.

For more information, please refer to the documentation:

[http://offline-digital-library.readthedocs.org/](http://offline-digital-library.readthedocs.org/)

![Screenshot of built-in Intranet](docs/source/_static/screenshot1.jpeg)

## Development

All the distributed scripts are located in `src/` and are also executable from that location.

To build a redistributable installscripts.tar.gz:

```
$ make build
```

To run an individual script from `src/conf.d` on the current machine (supposing you are developing live on a server to setup):

```
cd src/
sudo ./install.sh <name of script>
```

### Bootstrapping a data drive

Not fully documented.
