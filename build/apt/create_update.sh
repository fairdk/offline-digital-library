#!/bin/bash

# Fixes a downloaded apt mirror
# See: https://github.com/apt-mirror/apt-mirror/issues/113#issuecomment-464932493

if [ -f /media/FAIR ]
then
	echo "expecting /media/FAIR"
	exit 1
fi

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

mkdir -p /media/FAIR/repository/apt-mirror

echo "Fetching Ubuntu netboot images.."

# Here's a nice function to only download if file size doesn't match remote server's
download_if_changed () {

  FILE_NAME="$1"
  FILE_URL="$2"

  local_file_size=$([[ -f ${FILE_NAME} ]] && wc -c < ${FILE_NAME} || echo "0")
  remote_file_size=$(curl -sI ${FILE_URL} | awk '/Content-Length/ { print $2 }' | tr -d '\r' )

  if [[ "$local_file_size" -ne "$remote_file_size" ]]; then
    curl -o ${FILE_NAME} ${FILE_URL}
  fi
}


cd /media/FAIR/repository
download_if_changed "netboot-amd64.tar.gz" "http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/netboot.tar.gz"
download_if_changed "netboot-i386.tar.gz" "http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-i386/current/images/netboot/netboot.tar.gz"
cd -

echo "Creating symlinks"
cd /media/FAIR/repository
if ! [ -e kolibri ]
then
  ln -s "apt-mirror/mirror/ppa.launchpad.net/learningequality/kolibri" kolibri
fi
cd -

mirrorlist="$SCRIPTPATH/mirror.list"

# Somehow this doesn't work, so set default below instead
dataFolder=$(grep -F "set base_path" $mirrorlist | tr -s " " | cut -d' ' -f4)

if [ "$dataFolder" == "" ]
then
	dataFolder="/media/FAIR/repository/apt-mirror/"
fi

# combined solutions for apt mirror from: https://github.com/apt-mirror/apt-mirror/issues/49,https://github.com/apt-mirror/apt-mirror/issues/102
# I had to create this file, in order to solve the issue of not downloading DEP-11 @2 files
# so all what I'm doing is running apt mirror manually, then downloading the other icon files every time
# Grep will grep the line starting with "set base_path"
# then we trim all extra white spaces
# then we cut the string by delimiter white space and take third value

echo "Base Folder path set in mirror.list is: $dataFolder"

apt-mirror "$mirrorlist"

echo
echo -n "Do you want to check MD5 sum and download failed (auto N in 5 seconds)? This can take a long time, but is recommended for large downloads [y/N]"
echo
read -t 5 answer
exit_status=$?
if [ $exit_status -ne 0 ] || [ "$answer" == "" ] || [ "$answer" == "n" ] || [ "$answer" == "N" ]
then
  echo "Skipping MD5."
else
    FAILEDPACKAGES=""
    echo "Reading and Checking MD5 checksum using file: $dataFolder/var/MD5"
    rm -f FAILED_MD5.txt
    echo "Failed File will be stored in: $(PWD)/FAILED_MD5.txt"
    while IFS='' read -r line || [[ -n "$line" ]]; do
        #echo "Checking: $line"
        sum=$(echo $line | cut -d' ' -f1)
        filename=$(echo $line | cut -d' ' -f2)
        echo "$sum $dataFolder/mirror/$filename" | md5sum -c -
        RESULT=$?
        if [ $RESULT -ne 0 ];then
            echo "$dataFolder/mirror/$filename" >> FAILED_MD5.txt
            wget -O $dataFolder/mirror/$filename $filename
            echo "$sum $dataFolder/mirror/$filename" | md5sum -c -
            SUBRESULT=$?
            if [ $SUBRESULT -ne 0 ];then
                echo "Sorry failed checksum again for file: $dataFolder/mirror/$filename"
                $FAILEDPACKAGES+="$dataFolder/mirror/$filename      Failed again, sorry cannot help"
            fi
        fi
    done < "$dataFolder/var/MD5"
    #md5sum -c $dataFolder/var/MD5 | grep --line-buffered -i "FAILED" | tee FAILED_MD5.txt
    echo $FAILEDPACKAGES
fi


echo -n "Do you want Download DEP-11 @ 2 icons (auto Y in 5 seconds)? [Y/n]"
echo
read -t 5 answer
exit_status=$?
ubuserv=$(grep -m1 ubuntu.com /etc/apt/mirror.list |awk '{print $2}' |cut -d '/' -f3)
if [ "$answer" != "${answer#[Yy]}" ] || [ "$answer" == "" ] || [ $exit_status -ne 0 ] ;then
    cd $dataFolder/mirror/${ubuserv}/ubuntu/dists
    downloadRoot="$dataFolder/mirror/${ubuserv}/ubuntu/dists"
    echo "Downloading fixes to $downloadRoot"
    cd $downloadRoot
    for dist in *; do 
    dist=$(basename $dist)
               for comp in main  multiverse universe; do
                               mkdir -p ${dist}/${comp}/dep11
        for size in 48 64 128; do
        wget http://${ubuserv}/ubuntu/dists/${dist}/${comp}/dep11/icons-${size}x${size}@2.tar.gz -O ${dist}/${comp}/dep11/icons-${size}x${size}@2.tar.gz;
          done
       done
    done
else
    echo "ok no downloading for icons"
fi


echo -n "Do you want to run Clean Script (auto Y in 5 seconds)? [Y/n]"
echo
read -t 5 answer
exit_status=$?
if [ "$answer" != "${answer#[Yy]}" ] || [ "$answer" == "" ] || [ $exit_status -ne 0 ] ;then
    $dataFolder/var/clean.sh
fi
