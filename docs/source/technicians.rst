Setting up a server
===================

Server requirements
-------------------

The following items are necessary for setting up a server:

* Data drive (at least 1 TB) preloaded with all of the data (if you don't have a data drive already, see these :ref:`instructions for creating one <cloning-ref-label>`) 
* A target computer, which should be a machine with a decent amount of memory (> 4 GB), fast CPU, and stable fans
* A USB flash containing default Ubuntu Desktop (18.04 64-bit)
* A USB flash containing the Offline Digital Library `installscripts <https://gitlab.com/fairdk/offline-digital-library>`__

  * If upgrading a previously installed site, copy the old configuration of the centre to a directory on the flash called *installscripts/config/local/name-of-site*

Installation procedure
----------------------

#. Install Ubuntu 18.04 64-bit on the server, choose to erase everything on the server's disk if there is already something there. Name the root user and choose a password that was agreed with the other technicians (for more discussion of security, see this :ref:`description of security policy <security-ref-label>`).
#. Shutdown the server after installation
#. Install the Data drive in the server case, connect it to a SATA port
#. Boot the system and log in
#. Insert USB flash with installation scripts
#. Make sure that the hard drive is mounted. You can use command "mount" or click the hard drive's icon in the application bar, bottom left
#. Now open a terminal, CTRL+ALT+T
#. Type ``sudo bash`` to get a command line with administration privileges
#. Go where the USB flash is mounted: ``cd /path/to/usb-flash``, for instance "/media/fair/installscripts".
#. Go to the "installscripts" folder, for instance, ``cd offline-digital-library``. This folder should contain "install.sh", the main program that transforms the newly installed Ubuntu desktop to an ICT centre server.
#. Run ``./install.sh`` and the installation begins. There will be promts to enter configuration data at the end of the installation, but the first part will take some minutes, so you can go for tea.
#. At the end, the installation asks for a "Local overlay", that means that specific configurations for this centre is added. It will ask about each folder that it finds in "installationscripts/config/local". This should include the centre, that you are working on.


Creating a new Local overlay
----------------------------

#. Go to the installscripts/config/local folder::

      bash
      cd installscripts/config/local

#. Look at what's there and select an existing centre as a template and copy it::

      cp -R other-centre new-centre

#. Re-run the last part of the install.sh job::

      cd ../../
      ./install.sh 98-local.sh


Changing the local configuration
________________________________


If you need to change files in the local configurations, you should read the documentation for creating local configurations (*which doesn't exist yet*). But the overall idea is this:

#. Make changes to the files
#. Re-run the last part of the install.sh job::

    cd /media/fair/usb-flash/path/to/installscripts/
    ./install.sh 98-local.sh


Re-running the local configuration on all machines
__________________________________________________


If you have already installed all client machines and have made ammendments to the local configuration (e.g. added a new standard program, user or printer driver), you can re-run the post-installion by running this command on the server::

    cd /media/fair/usb-flash/path/to/installscripts/
    python scripts/deploy/rerun_postinstall.py

.. warning:: Only machines that are switched on and correctly attached to the network will be affected.



After installing
----------------

Make sure that you have copied the contents of the installscripts to "/root/" so another technician can know what was used for the installation::

    cd /media/fair/usb-flash/path/to/installscripts/
    cp -R . /root/

Make sure to also copy the configuration overlay, for instance::

    cd /media/fair/usb-flash/path/to/installscripts/config/local
    cp -R . /root/

    
Installing clients (student computers)
--------------------------------------

Make sure that network booting with PXE is enabled in the BIOS. When you have successfully booted from the
network, you will be quickly prompted to choose what operating system to boot. Select the ``Automatic Ubuntu install`` option which will install
everything without asking questions.

After the installation is completed, the client will reboot
**but it's not yet finished!** The first time the client boots after installation, it will complete a *post-install* procedure, which is a custom script that will install and configure additional software.

This process takes time, so even if you do not see any progress on the screen
for up to 20 minutes, just let the script finish.


Machines that cannot boot from network
______________________________________

Occasionally, you may encounter a computer that is unable to boot with PXE boot. In this case you will need to create a bootable CD / USB flash with gPXE. Booting from this media will substitute for the missing/damaged on-board LAN PXE client. You can obtain the .iso file from:

Websites:

* ``https://rom-o-matic.eu/`` (currently down)
* ``http://ipxe.org/download``

Instructions:

* Copy the .iso image to a USB flash, this will overwrite everything of the USB:
* Insert flash, it may be automatically mounted so should be unmounted if it's mounted.

  * To see what is mount: ``mount``
  * To unmount: ``umount /dev/sdX1``
  * To copy: ``sudo dd bs=4M if=Downloads/gpxe.iso of=/dev/sdX``

* Now the flash is bootable

Otherwise, burn the .iso image to a CDR, but remember that you should burn an *image* not a file.


Verifying an installed client
-----------------------------

It's important to check that the client has installed everything correctly,
because an interruption during the unattended intallation will sometimes cause
computers to be in a bad state.

Verify for instance:

* When logging in as a student, you receive the Gnome Classic desktop
  interface, which has the Applications + Places menu in the lower left corner.
  If you are instead met with the default Unity desktop (with icons vertically along the left side), log back
  out and select Gnome Fallback (Metacity) instead from the menu next to the
  username selection.

* That the ``student`` and ``teacher`` account are setup correctly, and that there
  is no password required for the ``student`` account.

* KA Lite videos are playing correctly.

* That the folder with *Materials For Students* contains files from the server and teachers can
  write to this folder on the server.
