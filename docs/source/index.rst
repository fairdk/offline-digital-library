An Offline Digital Library
==========================

This project automatically configures a server for a Digital Library.
Client computers for students/users are then installed from software images on the server via PXE netboot.
The server contains a number of offline educational resources, together with
the full Ubuntu software repositories.

This documentation is intended for technicians working in the field to deploy
and maintain such Digital Libraries. 

.. warning::

    This is a **WORK IN PROGRESS** Ubuntu 18.04 (Bionic) version.
    It is not complete yet.

Contents
--------

.. toctree::
   :maxdepth: 2
   
   technicians
   checklist
   howtos/index
   troubleshooting
   background


This project captures the efforts of `FAIR`_ in Malawi. As the number
of ICT centres steadily grows, the project adds more efficiency and is improving its architecture to
accommodate development of new features and future management of individual centre setups.

Basically, these scripts deliver a fully-automated configuration of a default Ubuntu desktop
to transform it into a server for the ICT centres, provided that it has access to a *data drive*
containing a copy of the Ubuntu repositories. The server provides a fully functional Ubuntu repository
with thousands of programs, combined with TFTP services for installing client desktops over a LAN.

.. _FAIR: http://www.fairinternational.org

Offline resources
-----------------

In addition to LAN-managed installations of Ubuntu, the server can also provide a range of contemporary
offline projects, such as Kolibri and Kiwix (including Wikipedia), together with a *Intranet* service that collects 
various educational resources for access via HTTP over the local LAN.

.. image:: _static/screenshot1.jpeg
    :width: 300

.. image:: _static/screenshot2.jpeg
    :width: 300

.. image:: _static/screenshot3.jpeg
    :width: 300

.. image:: _static/screenshot4.jpeg
    :width: 300

.. image:: _static/screenshot5.jpeg
    :width: 300

.. image:: _static/screenshot6.jpeg
    :width: 300

.. image:: _static/screenshot7.jpeg
    :width: 300

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

